# mkapi


Instalation
----

Via composer:
```
composer require yamitec/mkapi
```

Or manually insert this block into your composer.json in require section:
```
"require": {
    "yamitec/mkapi": "dev-master", // <- this line
}
```


Basic Usage:
----

```
$php
use Exception;
use MikrotikAPI\Roar\Roar;
use MikrotikAPI\Entity\Auth;

// create a connection with Mikrotik Router

try {
        $auth = new Auth($config['host'], $config['port'], $config['user'], $config['pass']);
        $auth->setDebug(true)->setTimeout(10)->setDelay(5);
        $roar = new Roar($auth);
        if ($roar->isConnected()) {
            echo json_encode(["success" => true, "message" => "RB Conectada"]);
            return true;
        }
        echo json_encode(["success" => false, "message" => "RB Desconectada"]);
        return false;
    } catch (MikrotikException $e) {
        echo json_encode(["success" => false, "message" => $e->getMessage()]);
        return false;
    } catch (Exception $e) {
        echo json_encode(["success" => false, "message" => $e->getMessage()]);
        return false;
    }
```

Getting interfaces:
---
```
$php
use MikrotikAPI\Roar\Roar;
use MikrotikAPI\Commands\Interfaces;

    $auth = new Auth($config['host'], $config['port'], $config['user'], $config['pass']);
    $auth->setDebug(true)->setTimeout(10)->setDelay(5);
    $roar = new Roar($auth);
    if ($roar->isConnected()) {
        $iComm = new Interfaces($conn);
        $interfaces = $iComm->getAll() // returns all interfaces as array
        var_dump($interfaces);
    }
```



##### Created by Maou Yami
##### (cc) 2005
##### License: MIT
